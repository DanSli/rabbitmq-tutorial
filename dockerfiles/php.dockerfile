FROM php:8.0-fpm

WORKDIR /var/www/html

RUN adduser --disabled-password --gecos '' www

RUN apt update && apt install -y unzip curl

COPY nginx/www.conf /usr/local/etc/php-fpm.d/www.conf

RUN docker-php-ext-install pdo pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=2.1.9