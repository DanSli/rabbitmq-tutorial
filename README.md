# Example project Docker + Laravel 9 + php 8.0

### Run project:

- docker-compose build
- docker-compose up -d

### To use Laravel first you must:

- docker exec -ti php bash
- docker-php-ext-install sockets - https://stackoverflow.com/questions/1361925/how-to-enable-socket-in-php
- docker-compose update