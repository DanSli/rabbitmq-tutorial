<?php

use App\Http\Controllers\RabbitTutorialController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', [RegisterController::class, 'index']);

Route::prefix('tutorial')->group(function() {
    Route::prefix('zad1')->group(function() {
        Route::get('send', [RabbitTutorialController::class, 'zad1Send']);
        Route::get('receive', [RabbitTutorialController::class, 'zad1Receive']);
    });

    Route::prefix('zad2')->group(function() {
        Route::get('new-task', [RabbitTutorialController::class, 'zad2NewTask']);
        Route::get('worker', [RabbitTutorialController::class, 'zad2Worker']);
    });

    Route::prefix('zad3')->group(function() {
        Route::get('emit-log', [RabbitTutorialController::class, 'zad3EmitLog']);
        Route::get('receive-logs', [RabbitTutorialController::class, 'zad3ReceiveLogs']);
    });

    Route::prefix('zad4')->group(function() {
        Route::get('emit-log-direct', [RabbitTutorialController::class, 'zad4EmitLogDirect']);
        Route::get('receive-log-direct', [RabbitTutorialController::class, 'zad4ReceiveLogDirect']);
    });

    Route::prefix('zad4')->group(function() {
        Route::get('emit-log-direct', [RabbitTutorialController::class, 'zad4EmitLogDirect']);
        Route::get('receive-log-direct', [RabbitTutorialController::class, 'zad4ReceiveLogDirect']);
    });

    Route::prefix('zad5')->group(function() {
        Route::get('emit-log-topic', [RabbitTutorialController::class, 'zad5EmitLogTopic']);
        Route::get('receive-log-topic', [RabbitTutorialController::class, 'zad5ReceiveLogTopic']);
    });

    Route::prefix('zad6')->group(function() {
        Route::get('rpc-server', [RabbitTutorialController::class, 'zad6RPCServer']);
        Route::get('rpc-client', [RabbitTutorialController::class, 'zad6RPCClient']);
    });

    Route::get('stop', [RabbitTutorialController::class, 'stopQueue']);
});

