## RabbitMQ Tutorial: 
Link: https://www.rabbitmq.com/getstarted.html

- App\Services\Tutorial\Zad1Service;
- App\Services\Tutorial\Zad2Service;
- App\Services\Tutorial\Zad3Service;
- App\Services\Tutorial\Zad4Service;
- App\Services\Tutorial\Zad5Service;
- App\Http\Controllers\RabbitTutorialController

## Opis RabitMQ


#### queue_declare 

    Deklerowanie Kolejki

#### AMQPMessage

     przygotowanie wiadomści dla MQ

#### basic_publish

    Publikacja dla kolejki. Można dodać tutaj np. Wiadomość

#### basic_consume

    Czyli generuje Callback dla konsumenta. 
    Można tutaj dodać funkcję która wyświetla odebraną wiadomość

#### bacis_qos 

    To mówi RabbitMQ, aby nie przekazywał worker więcej niż jednej wiadomości na raz.

#### exchange_declare
    
    Ta metoda tworzy wymianę, jeśli jeszcze nie istnie, 
    a jeśli istnieje, sprawdza czy jest to poprawna i oczekiwana klasa.

    https://www.rabbitmq.com/tutorials/amqp-concepts.html

    Istnieją kilka typów exchange:  
        - Direct - Wymiana bezpośrednia dostarcza wiadomości do kolejek na podstawie klucza routingu wiadomości.
        - Topic - Wymiany tematyczne kierują wiadomości do jednej lub wielu kolejek na podstawie
    dopasowania klucza routingu wiadomości i wzorca użytego do powiązania kolejki z wymianą.
        - Headers - Wymiana nagłówków jest przeznaczona do routingu na wielu atrybutach, 
    które łatwiej wyrazić jako nagłówki wiadomości niż klucz routingu.
        - Fanout - kieruje wiadomości do wszystkich powiązanych z nią kolejek, a klucz routingu jest ignorowany.

#### queue_bind
    
    Relacja między "exchange" a "queue" nazywa się Bind.
