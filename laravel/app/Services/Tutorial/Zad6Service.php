<?php

namespace App\Services\Tutorial;

use App\Services\RabbitMQService;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * https://www.rabbitmq.com/tutorials/tutorial-six-php.html
 */
class Zad6Service
{
    private RabbitMQService $rabbitMQ;

    private $channel;
    private $callback_queue;
    private $response;
    private $corr_id;

    /**
     * @param RabbitMQService $rabbitMQ
     */
    public function __construct(RabbitMQService $rabbitMQ)
    {
        $this->rabbitMQ = $rabbitMQ;
        $this->channel = $this->rabbitMQ->getChannel();
    }

    /**
     * @return void
     */
    public function rpcClient(): void
    {
        list($this->callback_queue, ,) = $this->channel->queue_declare(
            "",
            false,
            false,
            true,
            false
        );
        $this->channel->basic_consume(
            $this->callback_queue,
            '',
            false,
            true,
            false,
            false,
            array(
                $this,
                'onResponse'
            )
        );

        $response = $this->call(30);
        Log::info(" [.] Got:");
        Log::info(json_encode($response));
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function rpcServer(): void
    {
        $this->channel->queue_declare(
            'rpc_queue',
            false,
            false,
            false,
            false
        );

        Log::info(' [x] Awaiting RPC requests');

        $callback = function ($req) {
            $n = intval($req->body);
            Log::info(' [.] fib('.$n.')');
            echo ' [.] fib(', $n, ")\n";

            $msg = new AMQPMessage(
                (string) $this->fib($n),
                array('correlation_id' => $req->get('correlation_id'))
            );

            $req->delivery_info['channel']->basic_publish(
                $msg,
                '',
                $req->get('reply_to')
            );
            $req->ack();
        };

        $this->channel->basic_qos(
            null,
            1,
            null
        );
        $this->channel->basic_consume(
            'rpc_queue',
            '',
            false,
            false,
            false,
            false,
            $callback
        );

        while ($this->channel->is_open()) {
            $this->channel->wait();
        }

        $this->rabbitMQ->close();
    }

    /**
     * @param $rep
     * @return void
     */
    public function onResponse($rep): void
    {
        if ($rep->get('correlation_id') == $this->corr_id) {
            $this->response = $rep->body;
        }
    }

    /**
     * @param $n
     * @return int
     */
    private function call($n): int
    {
        $this->response = null;
        $this->corr_id = uniqid();

        $msg = new AMQPMessage(
            (string) $n,
            array(
                'correlation_id' => $this->corr_id,
                'reply_to' => $this->callback_queue
            )
        );
        $this->channel->basic_publish($msg, '', 'rpc_queue');
        while (!$this->response) {
            Log::info('await');
            $this->channel->wait();
        }
        return intval($this->response);
    }

    /**
     * @param $n
     * @return int
     */
    private function fib($n): int
    {
        if ($n == 0) {
            return 0;
        }
        if ($n == 1) {
            return 1;
        }
        return $this->fib($n-1) + $this->fib($n-2);
    }
}
