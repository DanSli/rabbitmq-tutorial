<?php

namespace App\Services\Tutorial;

use App\Services\RabbitMQService;
use Exception;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * https://www.rabbitmq.com/tutorials/tutorial-one-php.html
 */
class Zad1Service
{
    /**
     * @var RabbitMQService
     */
    private RabbitMQService $rabbitMQ;

    /**
     * @param RabbitMQService $rabbitMQ
     */
    public function __construct(RabbitMQService $rabbitMQ)
    {
        $this->rabbitMQ = $rabbitMQ;
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function send(): void
    {
        $channel = $this->rabbitMQ->getChannel();

        $channel->queue_declare(
            'hello',
            false,
            false,
            false,
            false
        );

        $msg = new AMQPMessage('Hello World!');
        $channel->basic_publish(
            $msg,
            '',
            'hello'
        );


        Log::info("[x] 'Sent Hello World!'");

        $this->rabbitMQ->close();
    }

    /**
     * @return void
     * @throws Exception
     */
    public function receive(): void
    {
        $channel = $this->rabbitMQ->getChannel();

        $channel->queue_declare(
            'hello',
            false,
            false,
            false,
            false
        );

        $callback = function ($msg) {
            Log::info(json_encode($msg->body));
        };

        $channel->basic_consume(
            'hello',
            '',
            false,
            true,
            false,
            false,
            $callback
        );

        while ($channel->is_open()) {
            $channel->wait();
        }

        $this->rabbitMQ->close();
    }
}
