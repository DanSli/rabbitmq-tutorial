<?php

namespace App\Services\Tutorial;

use App\Services\RabbitMQService;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Message\AMQPMessage;

/**
 *  https://www.rabbitmq.com/tutorials/tutorial-five-php.html
 */
class Zad5Service
{
    /**
     * @var RabbitMQService
     */
    private RabbitMQService $rabbitMQ;

    /**
     * @param RabbitMQService $rabbitMQ
     */
    public function __construct(RabbitMQService $rabbitMQ)
    {
        $this->rabbitMQ = $rabbitMQ;
    }

    public function emitLogTopic(array $bindingKeys): void
    {
        $channel = $this->rabbitMQ->getChannel();

        $channel->exchange_declare(
            'topic_logs',
            'topic',
            false,
            false,
            false
        );

        foreach($bindingKeys as $key => $bindingKey) {
            $msg = new AMQPMessage($bindingKey);

            $channel->basic_publish($msg, 'topic_logs', $key);

            Log::info(' [x] Sent ' . $key . ':');
            Log::info(json_encode($bindingKey));
        }
    }

    public function receiveLogTopic(array $bindingKeys): void
    {
        $channel = $this->rabbitMQ->getChannel();

        $channel->exchange_declare(
            'topic_logs',
            'topic',
            false,
            false,
            false
        );

        list($queue_name, ,) = $channel->queue_declare(
            "",
            false,
            false,
            true,
            false
        );

        foreach ($bindingKeys as $bindingKey) {
            $channel->queue_bind($queue_name, 'topic_logs', $bindingKey);
        }

        $callback = function ($msg) {
            Log::info(' [x] ' . $msg->delivery_info['routing_key']. ':');
            Log::info(json_encode($msg->body));
        };

        $channel->basic_consume(
            $queue_name,
            '',
            false,
            true,
            false,
            false,
            $callback
        );

        while ($channel->is_open()) {
            $channel->wait();
        }
    }
}
