<?php

namespace App\Services\Tutorial;

use App\Services\RabbitMQService;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * https://www.rabbitmq.com/tutorials/tutorial-four-php.html
 */
class Zad4Service
{
    /**
     * @var RabbitMQService
     */
    private RabbitMQService $rabbitMQ;

    private const SEVERITIES = ["info", "warning", "error"];

    /**
     * @param RabbitMQService $rabbitM
     */
    public function __construct(RabbitMQService $rabbitM)
    {
        $this->rabbitMQ = $rabbitM;
    }

    /**
     * @param string $input
     * @return void
     */
    public function emitLogDirect(string $input): void
    {
        $channel = $this->rabbitMQ->getChannel();

        $severities = self::SEVERITIES;

        $channel->exchange_declare(
            'direct_logs',
            'direct',
            false,
            false,
            false
        );

        foreach($severities as $severity) {
            if (empty($severity)) {
                $severity = "Hello World!";
            }

            $msg = new AMQPMessage($severity);

            $channel->basic_publish($msg, 'direct_logs', $input);
        }

        Log::info(' [x] Sent ' . $input . ':');
        Log::info(json_encode($severities));
    }

    /**
     * @return void
     */
    public function receiveLogsDirect(): void
    {
        $channel = $this->rabbitMQ->getChannel();

        $severities = self::SEVERITIES;

        $channel->exchange_declare(
            'direct_logs',
            'direct',
            false,
            false,
            false
        );

        list($queue_name, ,) = $channel->queue_declare(
            "",
            false,
            false,
            true,
            false
        );

        foreach ($severities as $severity) {
            $channel->queue_bind($queue_name, 'direct_logs', $severity);
        }

        $callback = function ($msg) {
            Log::info(' [x] '. $msg->delivery_info['routing_key']. ':');
            Log::info(json_encode($msg->body));
        };

        $channel->basic_consume(
            $queue_name,
            '',
            false,
            true,
            false,
            false,
            $callback
        );

        while ($channel->is_open()) {
            $channel->wait();
        }
    }
}
