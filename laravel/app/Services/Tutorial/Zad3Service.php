<?php

namespace App\Services\Tutorial;

use App\Services\RabbitMQService;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * https://www.rabbitmq.com/tutorials/tutorial-three-php.html
 */
class Zad3Service
{
    /**
     * @var RabbitMQService
     */
    private RabbitMQService $rabbitMQ;

    /**
     * @param RabbitMQService $rabbitM
     */
    public function __construct(RabbitMQService $rabbitM)
    {
        $this->rabbitMQ = $rabbitM;
    }

    public function emitLog(array $messages): void
    {
        $channel = $this->rabbitMQ->getChannel();

        $channel->exchange_declare(
            'logs',
            'fanout',
            false,
            false,
            false
        );

        foreach ($messages as $message) {
            if (empty($message)) {
                Log::info("info: Hello World!");
            }

            $msg = new AMQPMessage($message);

            $channel->basic_publish($msg, 'logs');
        }
    }

    public function receiveLogs(): void
    {
        $channel = $this->rabbitMQ->getChannel();

        $channel->exchange_declare(
            'logs',
            'fanout',
            false,
            false,
            false
        );

        list($queue_name, ,) = $channel->queue_declare(
            "",
            false,
            false,
            true,
            false
        );

        $channel->queue_bind(
            $queue_name,
            'logs'
        );

        $callback = function ($msg) {
            Log::info(json_encode($msg->body));
            Log::info('[x] Done');
        };

        $channel->basic_consume(
            $queue_name,
            '',
            false,
            true,
            false,
            false,
            $callback
        );

        while ($channel->is_open()) {
            $channel->wait();
        }
    }
}
