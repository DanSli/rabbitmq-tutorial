<?php

namespace App\Services\Tutorial;

use App\Services\RabbitMQService;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * https://www.rabbitmq.com/tutorials/tutorial-two-php.html
 */
class Zad2Service
{
    /**
     * @var RabbitMQService
     */
    private RabbitMQService $rabbitMQ;

    /**
     * @param RabbitMQService $rabbitM
     */
    public function __construct(RabbitMQService $rabbitM)
    {
        $this->rabbitMQ = $rabbitM;
    }

    /**
     * @param array $messages
     * @return void
     * @throws \Exception
     */
    public function newTask(array $messages): void
    {
        $channel = $this->rabbitMQ->getChannel();

        $channel->queue_declare('task_queue', false, true, false, false);

        foreach ($messages as $message) {
            if (empty($message)) {
                $message = "Hello World!";
            }

            $msg = new AMQPMessage(
                $message,
                array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT)
            );

            $channel->basic_publish(
                $msg,
                '',
                'task_queue'
            );
        }

        $this->rabbitMQ->close();
    }

    /**
     * @param string $sessionName
     * @return void
     * @throws \Exception
     */
    public function worker(string $sessionName): void
    {
        $channel = $this->rabbitMQ->getChannel();

        $channel->queue_declare('task_queue', false, true, false, false);

        $callback = function ($msg) {
            sleep(substr_count($msg->body, '.'));
            Log::info(json_encode($msg->body));
            Log::info('[x] Done');
            $msg->ack();
        };

        $channel->basic_qos(
            null,
            1,
            null
        );

        $channel->basic_consume(
            'task_queue',
            '',
            false,
            false,
            false,
            false,
            $callback
        );

        while ($channel->is_open()) {
            Log::info($sessionName);
            $channel->wait();
        }

        $this->rabbitMQ->close();
    }
}
