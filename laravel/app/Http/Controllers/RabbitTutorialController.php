<?php

namespace App\Http\Controllers;

use App\Services\RabbitMQService;
use App\Services\Tutorial\Zad1Service;
use App\Services\Tutorial\Zad2Service;
use App\Services\Tutorial\Zad3Service;
use App\Services\Tutorial\Zad4Service;
use App\Services\Tutorial\Zad5Service;
use App\Services\Tutorial\Zad6Service;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RabbitTutorialController extends Controller
{
    private RabbitMQService $rabbitMQ;

    private Zad1Service $zad1Service;
    private Zad2Service $zad2Service;
    private Zad3Service $zad3Service;
    private Zad4Service $zad4Service;
    private Zad5Service $zad5Service;
    private Zad6Service $zad6Service;

    public function __construct()
    {
        $this->rabbitMQ = new RabbitMQService();
        $this->rabbitMQ->connect();

        $this->zad1Service = new Zad1Service($this->rabbitMQ);
        $this->zad2Service = new Zad2Service($this->rabbitMQ);
        $this->zad3Service = new Zad3Service($this->rabbitMQ);
        $this->zad4Service = new Zad4Service($this->rabbitMQ);
        $this->zad5Service = new Zad5Service($this->rabbitMQ);
        $this->zad6Service = new Zad6Service($this->rabbitMQ);
    }

    // ========================================
    // ========== Tutorial First PHP ==========
    // ========================================
    /**
     * @return Response
     * @throws \Exception
     */
    public function zad1Send(): Response
    {
        $this->zad1Service->send();

        return response('Sent');
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function zad1Receive(): Response
    {
        $this->zad1Service->receive();

        return response('Stopped');
    }

    // =========================================
    // ========== Tutorial Second PHP ==========
    // =========================================
    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function zad2NewTask(Request $request): Response
    {
        $messages = $request->input('messages');

        $this->zad2Service->newTask($messages);

        return response('Task sent');
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function zad2Worker(Request $request): Response
    {
        $name = $request->input('name');

        $this->zad2Service->worker($name);

        return response('Worker stopped');
    }

    // ========================================
    // ========== Tutorial Third PHP ==========
    // ========================================
    /**
     * @param Request $request
     * @return Response
     */
    public function zad3EmitLog(Request $request): Response
    {
        $messages = $request->input('messages');

        $this->zad3Service->emitLog($messages);

        return response('Log Emitted');
    }

    /**
     * @return Response
     */
    public function zad3ReceiveLogs(): Response
    {
        $this->zad3Service->receiveLogs();

        return response('Logs Received');
    }

    // =========================================
    // ========== Tutorial Fourth PHP ==========
    // =========================================
    /**
     * @return Response
     */
    public function zad4EmitLogDirect(Request $request): Response
    {
        $severity = $request->input('severity');

        $this->zad4Service->emitLogDirect($severity);

        return response('Direct Log Emitted');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function zad4ReceiveLogDirect(): Response
    {
        $this->zad4Service->receiveLogsDirect();

        return response('Direct Log Received');
    }

    // ========================================
    // ========== Tutorial Fifth PHP ==========
    // ========================================
    /**
     * @param Request $request
     * @return Response
     */
    public function zad5EmitLogTopic(Request $request): Response
    {
        $values = $request->input('values');

        $this->zad5Service->emitLogTopic($values);

        return response('Log Topic Emitted');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function zad5ReceiveLogTopic(Request $request): Response
    {
        $values = $request->input('values');

        $this->zad5Service->receiveLogTopic($values);

        return response('Receive Topic Emitted');
    }

    // ========================================
    // ========== Tutorial Sixth PHP ==========
    // ========================================
    /**
     * @return Response
     */
    public function zad6RPCServer(): Response
    {
        $this->zad6Service->rpcServer();

        return response('Run RPC Client');
    }

    /**
     * @return Response
     */
    public function zad6RPCClient(): Response
    {
        $this->zad6Service->rpcClient();

        return response('Run RPC Client');
    }

    // Others
    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function stopQueue(Request $request): Response
    {
        $queue = $request->input('queue');

        $channel = $this->rabbitMQ->getChannel();
        $channel->queue_delete(
            $queue
        );

        $this->rabbitMQ->close();

        return response('Queue: ' . $queue . ' stopped.');
    }
}
